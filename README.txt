TWubric

App initates from twibric-app.vue (misspelled in code) in the view folder.

twibric-app houses the layout of the app.
twibric-app is broken down into the following components:

    twibric-card.vue: Design of a twibric card is abstracted here. 
    twibric-sort.vue: Selecting key and order to sort twibric cards.
    date-picker.vue: Selecting date range for filtering twibric cards.
    key-actions.vue: Event listener for action-keys and design.
    visual-summary.vue: An alternate view for twibric data.


Data for the app is saved in data.json
Bootstrap is used for button group.
Fontawesome as the icon base.

Styling follows SCSS with BEM pattern.
Most styling is done without any external framework.